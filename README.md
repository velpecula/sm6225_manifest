# samsung_sm6225_manifest

## How to build the kernel
1. Intitalize your local repository using this manifest:
```
repo init -u https://gitlab.com/velpecula/samsung_sm6225/sm6225_manifest.git
```
2. Then to sync up:
```
repo sync
```
3. Build kernel:
```
./build.sh
